package android.pradok.crystalball;

public class Predictions {

    public static Predictions predictions;


    private String[] answers;

    private  Predictions()  {
        answers = new String[]  {
                "Your wishes will come true.",
                "Your wishes will NEVER ever come true."
                "POTATO."
        };
    }

    public static Predictions get() {
        if(predictions == null) {
            predictions = new Predictions();
        }
        return predictions;
    }

    public String getPredictions()  {
        return answers[0];
    }
}
